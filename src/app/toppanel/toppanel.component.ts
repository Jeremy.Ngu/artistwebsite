import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-toppanel',
    templateUrl: './toppanel.component.html',
    styleUrls: ['./toppanel.component.css']
})

export class ToppanelComponent implements OnInit {
    title = 'KYDO';
    constructor() { }
    
    ngOnInit() {
    }
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/