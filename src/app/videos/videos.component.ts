import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
    selector: 'app-videos',
    templateUrl: './videos.component.html',
    styleUrls: ['./videos.component.css']
})

export class VideosComponent implements OnInit {

    url = 'https://www.googleapis.com/youtube/v3/search?key=AIzaSyAM7ypRdH6oROexV9PmhxonfdbnHoeovJ0&channelId=UCzxL1Oq8N6tULSRoYuwCKwg&part=snippet,id&order=date&maxResults=5'
    videos_url = []

    constructor(private http: HttpClient) {
        this.http.get(this.url).toPromise().then(data => {
            
            console.log(data['items'])
            
            for (let video in data["items"]){

                this.videos_url.push( "https://www.youtube.com/embed/" + (data["items"][video]["id"]["videoId"]) )

            }
        })

    }   
    
    ngOnInit() {
    }
    
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/