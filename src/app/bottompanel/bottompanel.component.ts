import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-bottompanel',
    templateUrl: './bottompanel.component.html',
    styleUrls: ['./bottompanel.component.css']
})

export class BottompanelComponent implements OnInit {
    title = 'KYDO';
    constructor() { }
    
    ngOnInit() {
    }
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/