import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-apropos',
    templateUrl: './apropos.component.html',
    styleUrls: ['./apropos.component.css']
})

export class AproposComponent implements OnInit {
    title = 'Kydo';
    constructor() { }
    
    ngOnInit() {
    }
    
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/