import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-instrumentales',
    templateUrl: './instrumentales.component.html',
    styleUrls: ['./instrumentales.component.css']
})

export class InstrumentalesComponent implements OnInit {
    title = 'Kydo';
    constructor() { }
    
    ngOnInit() {
    }
    
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/