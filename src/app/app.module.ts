import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms'; 

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ToppanelComponent } from './toppanel/toppanel.component';
import { AccueilComponent } from './accueil/accueil.component';
import { VideosComponent } from './videos/videos.component';
import { InstrumentalesComponent } from './instrumentales/instrumentales.component';
import { ServicesComponent } from './services/services.component';
import { AproposComponent  } from './apropos/apropos.component';
import { BottompanelComponent } from './bottompanel/bottompanel.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatSliderModule } from '@angular/material/slider';
import {MatGridListModule} from '@angular/material/grid-list';
import {DemoMaterialModule} from './material-module';

import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [
    AppComponent,
    ToppanelComponent,
    AccueilComponent,
    VideosComponent,
    InstrumentalesComponent,
    ServicesComponent,
    AproposComponent,
    BottompanelComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    DemoMaterialModule,
    HttpClientModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
